import { component$, useStore } from '@builder.io/qwik';
import { useLocation } from '@builder.io/qwik-city';

interface HeaderProps {
  defaultSearchCategory?: string;
}


export default component$<HeaderProps>((props) => {
  const loc = useLocation();
  const s = useStore({
    searchText: "",
    searchType: ""
  })

  const defaultIsOperators = props.defaultSearchCategory != "events"

  return (
    <header>
      <div class="flex flex-col items-center justify-center border-b border-red-900 shadow-md">
        <a class="mb-4" href="/"><img src="/rmd-temp-logo.png" /></a>

        <div class="form-control justify-center w-5/6 max-w-2xl mb-4">
          <div class="input-group input-group-xs min-[321px]:input-group-sm justify-center items-center w-full">

            <select class="select select-bordered select-xs min-[321px]:select-sm">
              <option selected={defaultIsOperators}>Operators</option>
              <option selected={!defaultIsOperators}>Events</option>
            </select>

            <input type="text" placeholder="Search…" class="input flex-auto input-bordered input-xs min-[321px]:input-sm border-x-0" />

            <button class="btn btn-xs min-[321px]:btn-sm">GO</button>

          </div>
        </div>

      </div>
    </header>
  );
});
