import { component$ } from '@builder.io/qwik';
import type { DocumentHead } from '@builder.io/qwik-city';

export default component$(() => {
  return (
    <div class="mt-4">
      <div class="text-2xl font-bold">National</div>
      <div class="text-md font-bold">Overview</div>

      <div class="card card-compact card-bordered border-b-4 border-blue-900 mt-4">
        <div class="card-body">
          <div class="flex justify-between text-xs font-bold">
            <div>Top Operators</div>
            <a href="/search/?search=alloperators">view all operators ❯</a>
          </div>

          <div class="overflow-x-auto">
            <table class="table table-compact w-full">
              <thead>
                <tr>
                  <th></th> 
                  <th>Name</th>
                  <th>State</th>
                  <th>License</th>
                  
                </tr>
              </thead> 
              <tbody>
                <tr class="hover">
                  <th>1</th> 
                  <td>Hiltos</td>
                  <td>MI</td>
                  <td>123-abc-789-xyz</td>
                  
                </tr>
                <tr class="hover">
                  <th>2</th> 
                  <td>Operator ABC</td>
                  <td>AK</td>
                  <td>abcdefg-98765</td>
                  
                </tr>
              </tbody>
            </table>
          </div>

        </div>

      </div>

      <div class="card card-compact card-bordered border-b-4 border-blue-900 mt-4 h-96">
        <div class="card-body">
          <div class="flex justify-between text-xs font-bold">
            <div>Recent Events</div>
            <a href="/search/?search=allevents">view all events ❯</a>
          </div>

        </div>

      </div>
      
    </div>
  );
});

export const head: DocumentHead = {
  title: 'Regulator Dashboard'
};
