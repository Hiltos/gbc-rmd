import { component$, Slot, useStore } from '@builder.io/qwik';
import Header from '../components/header/header';
import Footer from '../components/footer/footer';

export default component$(() => {

  const s = useStore({
    jurisdiction: "National",
    operator: "",
    event: "",
    searchText: "",
    searchType: ""
  })

  return (
    <>
      <main>
        <Header />

        <section>
          <div class="flex flex-col items-center w-full mt-2">
            <div class="flex items-center justify-between w-full mt-4">
              <div class="mr-2 w-1/4 text-end text-sm">Jurisdiction</div>
              <div class="w-3/4">
                <select class="select select-bordered select-xs min-[321px]:select-sm w-full">
                  <option selected>National</option>
                  <option>Michigan</option>
                </select>
              </div>
            </div>

            <div class="flex items-center justify-between w-full mt-2">
              <div class="mr-2 w-1/4 text-end text-sm">Operator</div>
              <div class="w-3/4">
                <select class="select select-bordered select-xs min-[321px]:select-sm w-full">
                  <option selected>Select Operator</option>
                  <option>Operator ABC</option>
                </select>
              </div>
            </div>

            <div class="flex items-center justify-between w-full mt-2">
              <div class="mr-2 w-1/4 text-end text-sm">Event</div>
              <div class="w-3/4">
                <select class="select select-bordered select-xs min-[321px]:select-sm w-full">
                  <option selected>Select Event</option>
                  <option>Event XYZ</option>
                </select>
              </div>
            </div>
          </div>
          <Slot />
        </section>

      </main>

      <Footer />

    </>
  );
});
