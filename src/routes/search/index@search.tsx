import { component$ } from '@builder.io/qwik';
import { useLocation } from '@builder.io/qwik-city';

export default component$(() => {
  const loc = useLocation();

  return (
    <div class="mt-4">
      <div class="text-2xl font-bold">Search</div>
      {loc.url.searchParams.has("search") && loc.url.searchParams.get("search") == "alloperators" ? (
        <>
        <div class="text-md font-bold">All operators</div>

        <div class="card card-compact card-bordered border-b-4 border-blue-900 mt-4 h-96">
          <div class="card-body">
            <div class="flex justify-start text-xs font-bold">
              <div>Operator List</div>
            </div>
          </div>
        </div>
        </>
      ) : ``}

      {loc.url.searchParams.has("search") && loc.url.searchParams.get("search") == "allevents" ? (
        <>
        <div class="text-md font-bold">All events</div>

        <div class="card card-compact card-bordered border-b-4 border-blue-900 mt-4 h-96">
          <div class="card-body">
            <div class="flex justify-start text-xs font-bold">
              <div>Event List</div>
            </div>
          </div>
        </div>
        </>
      ) : ``}

      {loc.url.searchParams.has("search") &&
       loc.url.searchParams.has("type") &&
       loc.url.searchParams.get("type") == "event" ? (
        <>
        <div class="text-md font-bold">Event: "{loc.url.searchParams.get("search")}"</div>

        <div class="card card-compact card-bordered border-b-4 border-blue-900 mt-4 h-96">
          <div class="card-body">
            <div class="flex justify-start text-xs font-bold">
              <div>Event</div>
            </div>
          </div>
        </div>
        </>
      ) : ``}

      {loc.url.searchParams.has("search") &&
       loc.url.searchParams.has("type") &&
       loc.url.searchParams.get("type") == "operator" ? (
        <>
        <div class="text-md font-bold">Operator: "{loc.url.searchParams.get("search")}"</div>

        <div class="card card-compact card-bordered border-b-4 border-blue-900 mt-4 h-96">
          <div class="card-body">
            <div class="flex justify-start text-xs font-bold">
              <div>Operator</div>
            </div>
          </div>
        </div>
        </>
      ) : ``}
      
      
    </div>
  );
});